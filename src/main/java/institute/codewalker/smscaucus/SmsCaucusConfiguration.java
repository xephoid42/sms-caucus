package institute.codewalker.smscaucus;


import institute.codewalker.smscaucus.config.SmsFirBaseConfig;
import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.client.JerseyClientConfiguration;

public class SmsCaucusConfiguration extends Configuration {
    @Valid
    @NotNull
    @JsonProperty
    private JerseyClientConfiguration httpClient = new JerseyClientConfiguration();

    @NotNull
    @JsonProperty
    private String applicationUrl;

    @NotNull
    @JsonProperty
    private SmsFirBaseConfig firebase = new SmsFirBaseConfig();

    public SmsFirBaseConfig getFirebaseConfiguration() {
        return firebase;
    }

    public JerseyClientConfiguration getJerseyClientConfiguration() {
        return httpClient;
    }

    public String getApplicationUrl() { return applicationUrl; }
}