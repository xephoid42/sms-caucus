package institute.codewalker.smscaucus.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zekes on 3/2/2018.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class ActionNetworkTaggingResult {

    @JsonProperty("total_pages")
    private Integer totalPages;

    @JsonProperty("per_page")
    private Integer perPage;

    @JsonProperty("page")
    private Integer page;

    @JsonProperty("total_records")
    private Integer totalRecords;

    @JsonProperty("_embedded")
    private Embedded embedded;

    public List<String> getPersonLinks() {
        List<String> links = new ArrayList<>();
        for (ActionNetworkTagging tagging: embedded.taggings) {
            links.add(tagging.getPersonLink());
        }
        return links;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }

    public Embedded getEmbedded() {
        return embedded;
    }

    public List<ActionNetworkTagging> getTaggings() {
        return embedded.getTaggings();
    }

    public class Embedded {
        @JsonProperty("osdi:taggings")
        List<ActionNetworkTagging> taggings;

        public void setTaggings(List<ActionNetworkTagging> taggings) {
            this.taggings = taggings;
        }

        public List<ActionNetworkTagging> getTaggings() {
            return taggings;
        }
    }
}
