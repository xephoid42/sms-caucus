package institute.codewalker.smscaucus.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by zekes on 1/31/2018.
 */
public class SMSActivist {
    private String firstname;
    private String lastname;
    private String phone;

    public SMSActivist() { }

    public SMSActivist(String firstname, String lastname, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
    }

    @JsonProperty
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @JsonProperty
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
