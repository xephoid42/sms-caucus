package institute.codewalker.smscaucus.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zekes on 3/25/2018.
 */
@XmlRootElement
public class SMSFlow implements Serializable {
    private String apiKey;
    private Integer id;
    private Boolean active;
    private String title;
    private String fromNumber;
    private String activationKeyword;
    private String foreignPath;
    private String foreignExtraParams;
    private String foreignMethod;
    private Long created;
    private List<SMSFlowStep> steps;

    public SMSFlow() {
        steps = new ArrayList<>();
    }

    public SMSFlow(List<SMSFlowStep> steps) {
        this.steps = steps;
    }

    @JsonProperty
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty
    public String getActivationKeyword() {
        return activationKeyword;
    }

    public void setActivationKeyword(String activationKeyword) {
        this.activationKeyword = activationKeyword;
    }

    @JsonProperty
    public Integer getStepCount() {
        return steps.size();
    }

    @JsonProperty
    public String getForeignPath() {
        return foreignPath;
    }

    public void setForeignPath(String foriegnPath) {
        this.foreignPath = foriegnPath;
    }

    @JsonProperty
    public String getForeignExtraParams() {
        return foreignExtraParams;
    }

    public void setForeignExtraParams(String foreignExtraParams) {
        this.foreignExtraParams = foreignExtraParams;
    }

    @JsonProperty
    public String getForeignMethod() {
        return foreignMethod;
    }

    public void setForeignMethod(String foriegnMethod) {
        this.foreignMethod = foriegnMethod;
    }

    @JsonProperty
    public List<SMSFlowStep> getSteps() {
        return steps;
    }

    public void setSteps(List<SMSFlowStep> steps) {
        this.steps = steps;
    }

    @JsonProperty
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty
    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }
}