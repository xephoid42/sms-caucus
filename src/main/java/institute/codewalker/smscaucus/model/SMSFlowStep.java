package institute.codewalker.smscaucus.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by zekes on 3/25/2018.
 */
public class SMSFlowStep {
    private Integer stepNumber;
    private String prompt;
    private String foreignName;

    @JsonProperty
    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    @JsonProperty
    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    @JsonProperty
    public String getForeignName() {
        return foreignName;
    }

    public void setForeignName(String foriegnName) {
        this.foreignName = foriegnName;
    }
}