package institute.codewalker.smscaucus.model;

/**
 * Created by zekes on 3/23/2018.
 */
public class BulkSendResponse {
    private int step;
    private String from;
    private String responseText;
}