package institute.codewalker.smscaucus.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties({ "identifiers" })
public class ActionNetworkTagging {
    @JsonProperty("created_date")
    private Date createdDate;

    @JsonProperty("modified_date")
    private Date modifiedDate;

    @JsonProperty("item_type")
    private String itemType;

    @JsonProperty("_links")
    private Links _links;

    public String getPersonLink() {
        return _links.getPerson().getPersonLink();
    }

    @JsonIgnoreProperties({ "self", "osdi:tag" })
    public class Links {
        @JsonProperty("osdi:person")
        Person person;

        public class Person {
            @JsonProperty("href")
            private String personLink;

            public String getPersonLink() {
                return personLink;
            }

            public void setPersonLink(String link) {
                this.personLink = link;
            }
        }

        public Person getPerson() {
            return person;
        }

        public void setPerson(Person person) {
            this.person = person;
        }
    }
}