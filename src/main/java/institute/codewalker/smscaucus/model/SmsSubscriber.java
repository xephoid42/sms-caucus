package institute.codewalker.smscaucus.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by zekes on 10/3/2017.
 */
public class SmsSubscriber {
    private String phoneNumber;
    private String name;
    private String email;
    private String zipcode;

    private Integer step = 0;

    public SmsSubscriber() {
    }

    public SmsSubscriber(String phoneNumber, String name, String email, String zipcode) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.email = email;
        this.zipcode = zipcode;
    }

    @JsonProperty
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public String getEmail() {
        return email;
    }

    @JsonProperty
    public String getZipcode() {
        return zipcode;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Integer getStep() {
        return step;
    }

    public void resetStep() {
        this.step = 0;
    }

    public void nextStep() {
        this.step = this.step + 1;
    }
}
