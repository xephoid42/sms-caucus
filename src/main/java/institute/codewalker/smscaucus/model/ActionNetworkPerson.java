package institute.codewalker.smscaucus.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ActionNetworkPerson {

    @JsonProperty("given_name")
    private String givenName;

    @JsonProperty("family_name")
    private String familyName;

    @JsonProperty("email_addresses")
    private List<EmailAddress> emailAddresses;

    @JsonProperty("custom_fields")
    private CustomFields customFields;

    public String getPhoneNumber() {
        return customFields.getPhoneNumber();
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public List<EmailAddress> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(List<EmailAddress> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public CustomFields getCustomFields() {
        return customFields;
    }

    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }

    static class EmailAddress {
        @JsonProperty("primary")
        private Boolean primary;

        @JsonProperty("address")
        private String address;

        @JsonProperty("status")
        private String status;

        public Boolean getPrimary() {
            return primary;
        }

        public void setPrimary(Boolean primary) {
            this.primary = primary;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown=true)
    public class CustomFields {
        @JsonProperty("Phone Number")
        private String phoneNumber;

        @JsonProperty("Twitter Handle")
        private String twitterHandle;

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getTwitterHandle() {
            return twitterHandle;
        }

        public void setTwitterHandle(String twitterHandle) {
            this.twitterHandle = twitterHandle;
        }
    }
}
