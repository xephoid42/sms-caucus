package institute.codewalker.smscaucus;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredential;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import institute.codewalker.smscaucus.health.SimpleHealthCheck;
import institute.codewalker.smscaucus.resource.*;
import institute.codewalker.smscaucus.service.ActionNetworkService;
import institute.codewalker.smscaucus.service.FirebaseCurlService;
import institute.codewalker.smscaucus.utils.ApiKeyValidator;
import institute.codewalker.smscaucus.utils.BulkSendStatusMap;
import institute.codewalker.smscaucus.utils.SMSFlowMap;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.Client;
import java.io.InputStream;

public class SmsCaucusApp extends Application<SmsCaucusConfiguration> {

    public static void main(String[] args) throws Exception {
        new SmsCaucusApp().run(args);
    }

    @Override
    public String getName() {
        return "sms-caucus";
    }

    @Override
    public void initialize(Bootstrap<SmsCaucusConfiguration> bootstrap) {
    }

    @Override
    public void run(SmsCaucusConfiguration config, Environment env) {
        try {
            final InputStream serviceAccountInputStream = SmsCaucusApp.class.getResourceAsStream(config.getFirebaseConfiguration().getServiceAccountKey());
            final FirebaseCredential firebaseCredential = FirebaseCredentials.fromCertificate(serviceAccountInputStream);
            final FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredential(firebaseCredential)
                    .setDatabaseUrl(config.getFirebaseConfiguration().getDatabaseUrl())
                    .build();

            FirebaseApp.initializeApp(options);

            // As an admin, the app has access to read and write all data, regardless of Security Rules
            final DatabaseReference subscribersRef = FirebaseDatabase
                    .getInstance()
                    .getReference("subscribers");

            final DatabaseReference bulkProcessesRef = FirebaseDatabase
                    .getInstance()
                    .getReference("bulkSendStats");

            final DatabaseReference validApiKeysRef = FirebaseDatabase
                    .getInstance()
                    .getReference("validApiKeys");

            final DatabaseReference smsFlowResponses = FirebaseDatabase
                    .getInstance()
                    .getReference("smsFlowResponses");


            final DatabaseReference smsFlowsRef = FirebaseDatabase.getInstance().getReference("smsFlows");

            final BulkSendStatusMap statsMap = new BulkSendStatusMap(bulkProcessesRef);
            final ApiKeyValidator apiKeyValidator = new ApiKeyValidator(validApiKeysRef);

            final Client client = new JerseyClientBuilder(env.metrics()).using(config.getJerseyClientConfiguration())
                    .using(env)
                    .build("http-client");
            client.property(ClientProperties.READ_TIMEOUT, 30000);

            final ActionNetworkService actionNetworkService = new ActionNetworkService(client);
            final FirebaseCurlService firebaseCurlService = new FirebaseCurlService(client, firebaseCredential,
                    config.getFirebaseConfiguration().getDatabaseUrl());

            final SMSFlowMap smsFlowMap = new SMSFlowMap(smsFlowsRef);

            final TwilioSMSResource twilioSMSResource = new TwilioSMSResource(client, subscribersRef);
            final SendTestSubscriber sendTestSubscriber = new SendTestSubscriber(client);
            final TwilioBulkSendResource twilioBulkSendResource = new TwilioBulkSendResource(actionNetworkService,
                    apiKeyValidator, statsMap);
            final CheckBulkSendStatsResource checkBulkSendStatsResource = new CheckBulkSendStatsResource(statsMap);
            final GetAllStatsResource getAllStatsResource = new GetAllStatsResource(statsMap);

            final SimpleHealthCheck healthCheck = new SimpleHealthCheck();

            final TwilioTestSendText twilioTestSendText = new TwilioTestSendText();
            final SMSFlowResource smsFlowResource = new SMSFlowResource(smsFlowMap);
            final TwilioCallBackResource twilioCallBackResource = new TwilioCallBackResource(smsFlowMap, smsFlowResponses,
                    firebaseCurlService, apiKeyValidator, config.getApplicationUrl());

            env.healthChecks().register("simple", healthCheck);
            env.jersey().register(twilioSMSResource);
            env.jersey().register(sendTestSubscriber);
            env.jersey().register(twilioTestSendText);
            env.jersey().register(twilioBulkSendResource);
            env.jersey().register(checkBulkSendStatsResource);
            env.jersey().register(getAllStatsResource);
            env.jersey().register(smsFlowResource);
            env.jersey().register(twilioCallBackResource);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}