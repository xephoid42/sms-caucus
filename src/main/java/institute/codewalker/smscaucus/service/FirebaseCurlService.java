package institute.codewalker.smscaucus.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseCredential;
import com.google.firebase.auth.GoogleOAuthAccessToken;
import com.google.firebase.internal.NonNull;
import com.google.firebase.tasks.OnCompleteListener;
import com.google.firebase.tasks.Task;
import institute.codewalker.smscaucus.model.SMSResponse;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by zekes on 4/4/2018.
 */
public class FirebaseCurlService {

    private Client httpClient;
    private String accessToken;
    private ObjectMapper jsonObjectMapper;
    private FirebaseCredential firebaseCredential;
    private Timer accessTokenCollectorTimer = new Timer();
    private String databaseUrl;

    public FirebaseCurlService(Client httpClient, FirebaseCredential firebaseCredential, String databaseUrl) {
        this.httpClient = httpClient;
        this.firebaseCredential = firebaseCredential;
        this.jsonObjectMapper = new ObjectMapper();
        this.databaseUrl = databaseUrl;

        // Pull a new access token every hour
        accessTokenCollectorTimer.scheduleAtFixedRate(new AccessTokenCollector(this), 0, 60 * 60 * 1000);
    }

    public SMSResponse getSMSResponse(String key) throws IOException {
        String rawJson = this._apiGet("smsFlowResponses/" + urlEncode(key) + ".json", null);
        if (rawJson != null) {
            return jsonObjectMapper.readValue(rawJson, SMSResponse.class);
        } else {
            return null;
        }
    }

    private String _apiGet(String endpoint, MultivaluedStringMap params) {
        if (params == null) {
            params = new MultivaluedStringMap();
        }
        params.add("access_token", this.accessToken);
        WebTarget target = httpClient.target(databaseUrl).path(endpoint);

        if (params != null) {
            for (String key: params.keySet()) {
                target = target.queryParam(key, params.get(key).get(0));
            }
        }

        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);

        String response = null;
        try {
            Future<String> future = builder.async().get(String.class);
            do {
                Thread.sleep(100);
            } while (!future.isDone()); // wait until the future is done
            response = future.get();
        } catch (InternalServerErrorException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return response;
    }

    private String urlEncode(String toEncode) {
        String result;
        try {
            result = URLEncoder.encode(toEncode, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            result = toEncode;
        }
        return result;
    }

    private void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    class AccessTokenCollector extends TimerTask {
        private FirebaseCurlService service;
        public AccessTokenCollector(FirebaseCurlService service) {
            this.service = service;
        }

        @Override
        public void run() {
            firebaseCredential.getAccessToken().addOnCompleteListener(new OnCompleteListener<GoogleOAuthAccessToken>() {
                @Override
                public void onComplete(@NonNull Task<GoogleOAuthAccessToken> task) {
                    String accessToken = task.getResult().getAccessToken();
                    System.out.println("Got Access token: " + accessToken);
                    service.setAccessToken(accessToken);
                }
            });
        }
    }

}
