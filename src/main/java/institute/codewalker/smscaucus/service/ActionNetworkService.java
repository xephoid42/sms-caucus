package institute.codewalker.smscaucus.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import institute.codewalker.smscaucus.model.ActionNetworkPerson;
import institute.codewalker.smscaucus.model.ActionNetworkTaggingResult;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by zekes on 3/2/2018.
 */
public class ActionNetworkService {

    public static final String ACTION_NETWORK_API_URL = "https://actionnetwork.org/api/v2/";

    private Client httpClient;
    private String apiKey;
    private ObjectMapper jsonObjectMapper;

    public ActionNetworkService(Client httpClient) {
        this.httpClient = httpClient;
        this.jsonObjectMapper = new ObjectMapper();
    }

    public ActionNetworkTaggingResult getTaggings(String tagId, Integer page) throws IOException {
        MultivaluedStringMap params = new MultivaluedStringMap();
        params.add("page", page.toString());
        String json = this._apiGet("tags/" + tagId + "/taggings", params);
        if (json != null) {
            return jsonObjectMapper.readValue(json, ActionNetworkTaggingResult.class);
        } else {
            return null;
        }
    }

    public ActionNetworkPerson getPerson(String personId) throws IOException {
        String json = this._apiGet("people/" + personId, null);
        if (json != null) {
            return jsonObjectMapper.readValue(json, ActionNetworkPerson.class);
        } else {
            return null;
        }
    }

    private String _apiGet(String endpoint, MultivaluedStringMap params) {
        WebTarget target = httpClient.target(ACTION_NETWORK_API_URL).path(endpoint);

        if (params != null) {
            for (String key: params.keySet()) {
                target = target.queryParam(key, params.get(key).get(0));
            }
        }

        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);
        builder.header("OSDI-API-Token", this.apiKey);

        String response = null;
        try {
            Future<String> future = builder.async().get(String.class);
            do {
                Thread.sleep(100);
            } while (!future.isDone()); // wait until the future is done
            response = future.get();
        } catch (InternalServerErrorException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return response;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }
}