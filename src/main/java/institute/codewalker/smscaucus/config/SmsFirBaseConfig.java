package institute.codewalker.smscaucus.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SmsFirBaseConfig {

    private String serviceAccountKey;
    private String databaseUrl;

    public SmsFirBaseConfig() { }

    public SmsFirBaseConfig(String serviceAccountKey, String databaseUrl) {
        this.serviceAccountKey = serviceAccountKey;
        this.databaseUrl = databaseUrl;
    }

    @JsonProperty
    public String getServiceAccountKey() {
        return serviceAccountKey;
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }
}