package institute.codewalker.smscaucus.health;

import com.codahale.metrics.health.HealthCheck;

/**
 * Created by zekes on 10/3/2017.
 */
public class SimpleHealthCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }
}