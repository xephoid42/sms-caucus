package institute.codewalker.smscaucus;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.ext.util.IdentityHashMap;
import institute.codewalker.smscaucus.service.ActionNetworkService;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.jersey.validation.DropwizardConfiguredValidator;
import io.dropwizard.logging.SyslogAppenderFactory;
import io.dropwizard.setup.Environment;
import org.hibernate.validator.internal.engine.ValidatorImpl;

import javax.ws.rs.client.Client;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zekes on 3/2/2018.
 */
public class ANTester {

    public static void main(String[] argv) {

        Map<UUID, Date> test = new HashMap<>();

        UUID id = UUID.randomUUID();
        test.put(id, new Date());
        String idString = id.toString();

        System.out.println(id.hashCode() == (UUID.fromString(idString).hashCode()));
        System.out.println(test.get(UUID.fromString(idString)));
    }
}
