package institute.codewalker.smscaucus.utils;

import com.google.firebase.database.*;
import institute.codewalker.smscaucus.model.SMSFlow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Created by zekes on 3/25/2018.
 */
public class SMSFlowMap {

    private DatabaseReference smsFlowRef;
    private Map<String, List<SMSFlow>> flowsByApiKey;

    public SMSFlowMap(DatabaseReference smsFlowRef) {
        this.flowsByApiKey = new HashMap<>();
        this.smsFlowRef = smsFlowRef;

        this.smsFlowRef.addChildEventListener(new ChildEventListener() {

            private void updateFlowsByApiKey(DataSnapshot dataSnapshot) {
                if (!flowsByApiKey.containsKey(dataSnapshot.getKey())) {
                    flowsByApiKey.put(dataSnapshot.getKey(), new ArrayList<>());
                }
                List<SMSFlow> flows = flowsByApiKey.get(dataSnapshot.getKey());
                flows.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    SMSFlow flow = child.getValue(SMSFlow.class);
                    flows.add(flow);
                }
            }

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                this.updateFlowsByApiKey(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                this.updateFlowsByApiKey(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                this.updateFlowsByApiKey(dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                this.updateFlowsByApiKey(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // TODO: What does this mean?
            }
        });
    }

    public SMSFlow getActiveFlow(String apiKey) {
        SMSFlow result = null;
        if (flowsByApiKey.containsKey(apiKey)) {
            List<SMSFlow> flows = flowsByApiKey.get(apiKey);
            for(SMSFlow flow: flows) {
                if (flow != null && flow.getActive()) {
                    result = flow;
                }
            }
        }
        return result;
    }

    public List<SMSFlow> get(String apiKey) {
        return flowsByApiKey.get(apiKey);
    }

    public Boolean containsKey(String apiKey) {
        return flowsByApiKey.containsKey(apiKey);
    }

    public void put(String apiKey, List<SMSFlow> flows) {
        flowsByApiKey.put(apiKey, flows);
    }

    public void save() {
        smsFlowRef.setValue(flowsByApiKey);
    }
}
