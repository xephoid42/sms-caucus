package institute.codewalker.smscaucus.utils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by zekes on 3/6/2018.
 */
public class StringUtils {
    public static final String replaceValues(final String template,
                                 final Map<String, String> values){
        final StringBuffer sb = new StringBuffer();
        final Pattern pattern =
                Pattern.compile("\\[\\[(.*?)\\]\\]", Pattern.DOTALL);
        final Matcher matcher = pattern.matcher(template);
        while(matcher.find()){
            final String key = matcher.group(1);
            final String replacement = values.get(key);
            if(replacement == null){
                throw new IllegalArgumentException(
                        "Template contains unmapped key: "
                                + key);
            }
            matcher.appendReplacement(sb, replacement);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}