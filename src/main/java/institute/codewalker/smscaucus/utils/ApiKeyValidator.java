package institute.codewalker.smscaucus.utils;

import com.google.firebase.database.*;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Created by zekes on 3/23/2018.
 */
public class ApiKeyValidator {

    private Map<String, Boolean> keyMap;
    private DatabaseReference validApiKeysRef;

    public ApiKeyValidator(DatabaseReference ref) {
        validApiKeysRef = ref;
        keyMap = new HashMap<>();

        validApiKeysRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                keyMap.put(dataSnapshot.getKey(), dataSnapshot.getValue(Boolean.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                keyMap.put(dataSnapshot.getKey(), dataSnapshot.getValue(Boolean.class));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                keyMap.put(dataSnapshot.getKey(), null);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public Boolean isValid(String apiKey) {
        if (keyMap.containsKey(apiKey)) {
            return keyMap.get(apiKey);
        }
        return false;
    }
}