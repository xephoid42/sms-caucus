package institute.codewalker.smscaucus.utils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import institute.codewalker.smscaucus.model.BulkSendStats;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Created by zekes on 4/21/2018.
 */
public class BulkSendStatusMap {

    private Map<String, Map<String, BulkSendStats>> bulkSendStats;
    private DatabaseReference bulkSendStatsRef;

    public BulkSendStatusMap(DatabaseReference bulkSendStatsRef) {
        this.bulkSendStatsRef = bulkSendStatsRef;
        bulkSendStats = new HashMap<>();

        // Load the existing DB into memory...
        bulkSendStatsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getChildren().forEach(new Consumer<DataSnapshot>() {
                    @Override
                    public void accept(DataSnapshot dataSnapshot) {
                        Map<String, BulkSendStats> allStats = bulkSendStats.get(dataSnapshot.getKey());
                        if (allStats == null) {
                            allStats = new HashMap<>(); // allStats = [];
                            bulkSendStats.put(dataSnapshot.getKey(), allStats);// bulkSendStats[apiKey] = allStats;
                        }
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            allStats.put(child.getKey(), dataSnapshot.getValue(BulkSendStats.class));
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // ...
            }
        });
    }

    public BulkSendStats put(String apiKey, BulkSendStats stats) {
        Map<String, BulkSendStats> allStats = bulkSendStats.get(apiKey);
        if (allStats == null) {
            allStats = new HashMap<>();
            bulkSendStats.put(apiKey, allStats);
        }
        return allStats.put(stats.getId(), stats);
    }

    public Map<String, BulkSendStats> get(String apiKey) {
        return bulkSendStats.get(apiKey);
    }

    public void save() {
        bulkSendStatsRef.setValue(bulkSendStats);
    }
}