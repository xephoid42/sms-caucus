package institute.codewalker.smscaucus.resource;

import com.codahale.metrics.annotation.Timed;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import institute.codewalker.smscaucus.utils.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zekes on 2/28/2018.
 */
@Path("/send-test-text")
@Produces(MediaType.APPLICATION_JSON)
public class TwilioTestSendText {

    @POST
    @Timed
    public String sendTestText(@FormParam("tasid") String twilioAccountSid, @FormParam("tat") String twilioAuthToken,
                               @FormParam("tsid") String twilioServiceId, @FormParam("from") String from,
                               @FormParam("body") String body, @FormParam("to") String to) {
        if (twilioAccountSid == null || "".equals(twilioAccountSid)) {
            throw new WebApplicationException("Missing Twilio account Sid", 400);
        }

        if (twilioAuthToken == null || "".equals(twilioAuthToken)) {
            throw new WebApplicationException("Missing Twilio Auth Token", 400);
        }

        if ((twilioServiceId == null || "".equals(twilioServiceId)) && (from == null || "".equals(from))) {
            throw new WebApplicationException("Missing Twilio Service Id or from number", 400);
        }

        Twilio.init(twilioAccountSid, twilioAuthToken);

        Map<String, String> personValues = new HashMap<>();
        personValues.put("FIRSTNAME", "Jane");
        personValues.put("LASTNAME", "Doe");
        String personalMessage = StringUtils.replaceValues(body, personValues);

        Message message = null;

        if (twilioServiceId != null) {
            message = Message
                    .creator(new PhoneNumber(to), twilioServiceId, personalMessage)
                    .create();
        } else {
            message = Message
                    .creator(new PhoneNumber(to), new PhoneNumber(from), personalMessage)
                    .create();
        }

        if(message.getErrorCode() == null) { // null when sending is successful
            System.out.println("Successfully sent sms to: " + to);
            return "{ \"success\": true }";
        } else {
            System.out.println("Failed to send to: " + to +
                    ", please check your Twilio account dashboard for more information. ErrorCode " +
                    message.getErrorCode());
            throw new WebApplicationException("Failed to send to: " + to +
                    ",\n please check your Twilio account dashboard for more information. ErrorCode(" +
                    message.getErrorCode() + ")", 500);
        }
    }
}