package institute.codewalker.smscaucus.resource;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import institute.codewalker.smscaucus.model.BulkSendStats;
import institute.codewalker.smscaucus.service.ActionNetworkService;
import institute.codewalker.smscaucus.utils.ApiKeyValidator;
import institute.codewalker.smscaucus.utils.BulkSendStatusMap;
import institute.codewalker.smscaucus.workers.BulkSendWorker;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.UUID;


@Path("/bulk-send")
@Produces(MediaType.APPLICATION_JSON)
public class TwilioBulkSendResource {

    private ActionNetworkService actionNetwork;
    private ApiKeyValidator apiKeyValidator;
    private BulkSendStatusMap statsMap;

    public TwilioBulkSendResource(ActionNetworkService service, ApiKeyValidator validator,
                                  BulkSendStatusMap statsMap) {
        this.actionNetwork = service;
        this.apiKeyValidator = validator;
        this.statsMap = statsMap;
    }

    @POST
    @Timed
    public String bulkSend(@FormParam("apiKey") String apiKey, @FormParam("tasid") String twilioAccountSid, @FormParam("tat") String twilioAuthToken,
                           @FormParam("tsid") String twilioServiceId, @FormParam("from") String from,
                           @FormParam("anak") String actionNetworkApiKey, @FormParam("antid") String actionNetworkTagId,
                           @FormParam("body") String body) {
        if (apiKey == null || "".equals(apiKey)) {
            throw new WebApplicationException("Missing Action Texts API Key", 400);
        }
        if (!apiKeyValidator.isValid(apiKey)) {
            throw new WebApplicationException("Action Texts API Key was invalid!", 400);
        }
        if (twilioAccountSid == null || "".equals(twilioAccountSid)) {
            throw new WebApplicationException("Missing Twilio account Sid", 400);
        }

        if (twilioAuthToken == null || "".equals(twilioAuthToken)) {
            throw new WebApplicationException("Missing Twilio Auth Token", 400);
        }

        if ((twilioServiceId == null || "".equals(twilioServiceId)) && (from == null || "".equals(from))) {
            throw new WebApplicationException("Missing Twilio Service Id or from number", 400);
        }

        if (actionNetworkApiKey == null || "".equals(actionNetworkApiKey)) {
            throw new WebApplicationException("Missing Action Network API Key", 400);
        }

        if (actionNetworkTagId == null || "".equals(actionNetworkTagId)) {
            throw new WebApplicationException("Missing Action Network Tag ID", 400);
        }

        BulkSendStats stats = new BulkSendStats();
        UUID processId = UUID.randomUUID();
        stats.setApiKey(apiKey);
        stats.setId(processId.toString());
        stats.setErrors(0);
        stats.setMissingNumbers(0);
        stats.setMessagesSent(0);
        stats.setMessage(body);
        stats.setStarted(new Date());

        statsMap.put(processId.toString(), stats);
        statsMap.save();

        BulkSendWorker worker = new BulkSendWorker(twilioAccountSid, twilioAuthToken, twilioServiceId, from,
                actionNetworkApiKey, actionNetworkTagId, body, stats, actionNetwork, statsMap);

        worker.start();

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(stats);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new WebApplicationException(500);
        }
    }
}