package institute.codewalker.smscaucus.resource;

import com.codahale.metrics.annotation.Timed;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@Path("/send-test")
public class SendTestSubscriber {

    private Client httpClient;

    public SendTestSubscriber(Client httpClient) {
        this.httpClient = httpClient;
    }

    @GET
    @Timed
    public void sendTest(@QueryParam("name") String name, @QueryParam("email") String email, @QueryParam("zipcode") String zipcode, @QueryParam("phone") String phone) {
        MultivaluedStringMap formData = new MultivaluedStringMap();
        formData.putSingle("utf8", "✓");
        formData.putSingle("authenticity_token", "SpNuqbJPSfd09keh96ww//MziMC1f0MMzmc2g1qneN0=");
        formData.putSingle("answer[first_name]", name);
        formData.putSingle("answer[last_name]", "");
        formData.putSingle("answer[email]", email);
        formData.putSingle("answer[zip_code]", zipcode);
        formData.putSingle("Phone Number", phone);
        formData.putSingle("subscription[group]", "22328");
        formData.putSingle("subscription[sub_group_id]", "22328");
        formData.putSingle("subscription[http_referer]", "codewalker.institute");
        formData.putSingle("subscription[source]", "sms-caucus");

        WebTarget target = httpClient.target("https://actionnetwork.org/forms/join-from-sms").path("answers");
        Invocation.Builder builder = target.request(MediaType.APPLICATION_XHTML_XML);
        System.out.println(builder);

        try {
            String response = builder.post(Entity.form(formData), String.class);
            System.out.println(response);
        } catch (InternalServerErrorException e) {
            e.printStackTrace();
        }

    }
}
