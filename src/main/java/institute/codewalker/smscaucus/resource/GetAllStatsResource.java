package institute.codewalker.smscaucus.resource;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import institute.codewalker.smscaucus.model.BulkSendStats;
import institute.codewalker.smscaucus.utils.BulkSendStatusMap;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/check-all-stats")
@Produces(MediaType.APPLICATION_JSON)
public class GetAllStatsResource {
    private ObjectMapper mapper = new ObjectMapper();
    private BulkSendStatusMap statsMap;

    public GetAllStatsResource(BulkSendStatusMap statsMap) {
        this.statsMap = statsMap;
    }

    @GET
    @Timed
    public String getAllStats(@QueryParam("apiKey") String apiKey) {
        Map<String, BulkSendStats> allStats = statsMap.get(apiKey);
        if (allStats != null) {
            try {
                return mapper.writeValueAsString(allStats.values());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new WebApplicationException(500);
            }
        } else {
            throw new WebApplicationException(404);
        }
    }
}
