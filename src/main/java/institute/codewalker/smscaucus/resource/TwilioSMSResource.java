package institute.codewalker.smscaucus.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import institute.codewalker.smscaucus.model.SmsSubscriber;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.function.Consumer;

@Path("/sms-received")
@Produces(MediaType.APPLICATION_XML)
public class TwilioSMSResource {

    private Map<String, SmsSubscriber> subscribers = new HashMap<String, SmsSubscriber>();
    private Client httpClient;
    private DatabaseReference subscribersRef;

    public TwilioSMSResource(Client httpClient, DatabaseReference subscribersRef) {
        this.httpClient = httpClient;
        this.subscribersRef = subscribersRef;

        // Load the existing DB into memory...
        subscribersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getChildren().forEach(new Consumer<DataSnapshot>() {
                    @Override
                    public void accept(DataSnapshot dataSnapshot) {
                        subscribers.put(dataSnapshot.getKey(), dataSnapshot.getValue(SmsSubscriber.class));
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // ...
            }
        });
    }

    @POST
    @Timed
    public String doTheThing(
            @FormParam("Body") String body,
            @FormParam("From") String from) {
        if (!subscribers.containsKey(from)) {
            subscribers.put(from, new SmsSubscriber(from, null, null, null));
            // Save here to ensure whe get at least the phone number!
            subscribersRef.setValue(subscribers);
        }
        SmsSubscriber sub = subscribers.get(from);

        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        sb.append("<Response>");
        if (body.equals("ACTION")) {
            sub.resetStep();
            sb.append("<Message>Welcome to the Action Summit powered by Hip Hop Caucus &amp; Center for Civic Innovation. We'd like to get details to keep you in the know. What's your name?</Message>");
            sub.nextStep();
        } else if (sub.getStep() == 1) {
            sub.setName(body);
            sb.append("<Message>Hi " +body+ ", what's a good email for you?</Message>");
            sub.nextStep();
        } else if (sub.getStep() == 2) {
            sub.setEmail(body);
            sb.append("<Message>Great! What's your zipcode so we can keep you connected to engagement opportunities after the summit?</Message>");
            sub.nextStep();
        } else if (sub.getStep() == 3) {
            sub.setZipcode(body);
            sb.append("<Message>Thanks! We'll be sending you key event info throughout the Summit. Don't forget to share & tag events with @A3C @HipHopCaucus @civicatlanta</Message>");
            sub.nextStep();
            submit(sub);
        } else if (sub.getStep() == 4){
            sb.append("<Message>That's all for now! Stay tuned for more announcements.</Message>");
        } else {
            sb.append("<Message>If you would like to subscribe to Action Summit please text \"ACTION\" to this number.</Message>");
        }
        sb.append("</Response>");
        // Always save!
        subscribersRef.setValue(subscribers);
        return sb.toString();
    }

    public void submit(SmsSubscriber sub) {
        MultivaluedStringMap formData = new MultivaluedStringMap();
        formData.putSingle("utf8", "✓");
        formData.putSingle("authenticity_token", "SpNuqbJPSfd09keh96ww//MziMC1f0MMzmc2g1qneN0=");
        formData.putSingle("answer[first_name]", sub.getName());
        formData.putSingle("answer[last_name]", "");
        formData.putSingle("answer[email]", sub.getEmail());
        formData.putSingle("answer[zip_code]", sub.getZipcode());
        formData.putSingle("Phone Number", sub.getPhoneNumber());
        formData.putSingle("subscription[group]", "22328");
        formData.putSingle("subscription[sub_group_id]", "22328");
        formData.putSingle("subscription[http_referer]", "codewalker.institute");
        formData.putSingle("subscription[source]", "sms-caucus");

        WebTarget target = httpClient.target("https://actionnetwork.org/forms/join-from-sms/answers");
        Invocation.Builder builder = target.request(MediaType.APPLICATION_XML);

        try {
            String response = builder.post(Entity.form(formData), String.class);
            System.out.println(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}