package institute.codewalker.smscaucus.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.firebase.database.DatabaseReference;
import com.twilio.Twilio;
import com.twilio.base.ResourceSet;
import com.twilio.http.HttpMethod;
import com.twilio.rest.api.v2010.account.IncomingPhoneNumber;
import com.twilio.twiml.*;
import institute.codewalker.smscaucus.model.SMSFlow;
import institute.codewalker.smscaucus.model.SMSResponse;
import institute.codewalker.smscaucus.service.FirebaseCurlService;
import institute.codewalker.smscaucus.utils.ApiKeyValidator;
import institute.codewalker.smscaucus.utils.SMSFlowMap;
import io.dropwizard.jersey.PATCH;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zekes on 3/25/2018.
 */
@Path("/twilio-callback/{api-key}/")
@Produces(MediaType.APPLICATION_XML)
public class TwilioCallBackResource {

    private SMSFlowMap flowMap;
    private DatabaseReference flowResponsesRef;
    private FirebaseCurlService firebaseCurlService;
    private ApiKeyValidator apiKeyValidator;
    private String applicationUrl;

    public TwilioCallBackResource(SMSFlowMap flowMap, DatabaseReference flowResponsesRef,
                                  FirebaseCurlService firebaseCurlService, ApiKeyValidator apiKeyValidator,
                                  String applicationUrl) {
        this.flowMap = flowMap;
        this.flowResponsesRef = flowResponsesRef;
        this.firebaseCurlService = firebaseCurlService;
        this.apiKeyValidator = apiKeyValidator;
        this.applicationUrl = applicationUrl;
    }

    @PATCH
    @Timed
    public String setup(@PathParam("api-key") String apiKey, @FormParam("tasid") String twilioAccountSid,
                        @FormParam("tat") String twilioAuthToken) {
        if (twilioAccountSid == null || "".equals(twilioAccountSid)) {
            throw new WebApplicationException("Missing Twilio account Sid", 400);
        }

        if (twilioAuthToken == null || "".equals(twilioAuthToken)) {
            throw new WebApplicationException("Missing Twilio Auth Token", 400);
        }

        if (!apiKeyValidator.isValid(apiKey)) {
            throw new WebApplicationException("Invalid API key!", 401);
        }

        Integer count = 0;
        Twilio.init(twilioAccountSid, twilioAuthToken);

        ResourceSet<IncomingPhoneNumber> numbers = IncomingPhoneNumber.reader().read();

        String jsonNumbers = "[";
        for (IncomingPhoneNumber number : numbers) {
            count++;
            jsonNumbers += "\"" + number.getFriendlyName() + "\",";
            IncomingPhoneNumber.updater(number.getSid())
                    .setSmsUrl(applicationUrl + "/twilio-callback/" + apiKey)
                    .setSmsMethod(HttpMethod.GET).update();
        }
        if (count > 0) {
            jsonNumbers = jsonNumbers.substring(0, jsonNumbers.length() - 1);
        }
        jsonNumbers += "]";

        return "{ \"success\": true, \"numbers\": " +jsonNumbers+ " }";
    }

    @GET
    @Timed
    public String webhook(@PathParam("api-key") String apiKey, @QueryParam("Body") String body,
                          @QueryParam("From") String from) {
        if (!flowMap.containsKey(apiKey)) {
            throw new WebApplicationException(400);
        }
        SMSFlow activeFlow = flowMap.getActiveFlow(apiKey);
        String responseKey = apiKey + activeFlow.getId() + from;

        SMSResponse response = null;

        try {
            response = firebaseCurlService.getSMSResponse(responseKey);
        } catch (IOException e) {
            e.printStackTrace();
            throw new WebApplicationException("Could not read from Database", 500);
        }

        if (response == null) { // This is a new response
            response = new SMSResponse();
            response.setApiKey(apiKey);
            response.setFlowId(activeFlow.getId());
            response.setFrom(from);
            response.setCreated(System.currentTimeMillis());
            response.setStep(0);
        } if (body.toUpperCase().equals(activeFlow.getActivationKeyword().toUpperCase())) {
            response.setStep(0);
        }
        response.setUpdated(System.currentTimeMillis());

        List<String> responses = response.getResponses();
        if (responses == null || body.toUpperCase().equals(activeFlow.getActivationKeyword().toUpperCase())) {
            responses = new ArrayList<>();
        }
        if (response.getStep() <= activeFlow.getStepCount()) {
            responses.add(body);
        }
        response.setResponses(responses);

        Body reply = new Body(activeFlow.getSteps().get(response.getStep()).getPrompt());
        Message sms = new Message.Builder()
                .action(applicationUrl + "/twilio-callback/" + apiKey)
                .method(Method.POST)
                .to(from)
                .from(activeFlow.getFromNumber())
                .body(reply)
                .build();

        MessagingResponse twiml = new MessagingResponse.Builder().message(sms).build();

        if (response.getStep() < activeFlow.getStepCount()) {
            response.setStep(response.getStep() + 1);
        }

        // Save the thing
        flowResponsesRef.child(responseKey).setValue(response);

        String xml = null;
        try {
            xml = twiml.toXml();
        } catch (TwiMLException e) {
            e.printStackTrace();
            throw new WebApplicationException("Could not generate TwiML", 500);
        }

        return xml;
    }

    @POST
    @Timed
    public String smsHandler(@PathParam("apiKey") String apiKey, @QueryParam("MessageStatus ") String messageStatus,
                             @QueryParam("Body") String body, @QueryParam("From") String from) {
        System.out.println("Message send status: " + messageStatus);
        return "{\"success\": true }";
    }
}