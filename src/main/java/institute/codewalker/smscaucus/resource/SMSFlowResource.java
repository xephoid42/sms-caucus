package institute.codewalker.smscaucus.resource;

import com.codahale.metrics.annotation.Timed;
import institute.codewalker.smscaucus.model.SMSFlow;
import institute.codewalker.smscaucus.utils.SMSFlowMap;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zekes on 3/25/2018.
 */
@Path("/sms-flow/{api-key}/")
@Produces(MediaType.APPLICATION_JSON)
public class SMSFlowResource {

    SMSFlowMap flowMap;

    public SMSFlowResource(SMSFlowMap map) {
        this.flowMap = map;
    }

    @GET
    @Timed
    public List<SMSFlow> get(@PathParam("api-key") String apiKey, @QueryParam("fid") Integer flowId) {
        if (!flowMap.containsKey(apiKey)) {
            throw new WebApplicationException(404);
        } else {
            if (flowId == null || "".equals(flowId)) {
                return flowMap.get(apiKey);
            } else {
                List<SMSFlow> result = new ArrayList<>();
                List<SMSFlow> flows = flowMap.get(apiKey);
                if (flowId >= 0 && flowId < flows.size()) {
                    result.add(flows.get(flowId));
                    return result;
                } else {
                    throw new WebApplicationException(404);
                }
            }
        }
    }

    @POST
    @Timed
    public SMSFlow post(@PathParam("api-key") String apiKey, SMSFlow newFlow) {
        if (apiKey == null || "".equals(apiKey)) {
            throw new WebApplicationException("No api key set", 400);
        }
        newFlow.setApiKey(apiKey);
        newFlow.setCreated(System.currentTimeMillis());
        if (!flowMap.containsKey(apiKey)) {
            flowMap.put(apiKey, new ArrayList<>());
        }
        List<SMSFlow> flows = flowMap.get(apiKey);
        newFlow.setId(flows.size());
        flows.add(newFlow);
        flowMap.save();

        return newFlow;
    }

    @PUT
    @Timed
    public SMSFlow put(@PathParam("api-key") String apiKey, SMSFlow flow) {
        if (apiKey == null || "".equals(apiKey)) {
            throw new WebApplicationException("No api key set", 400);
        }
        if (!flowMap.containsKey(apiKey)) {
            throw new WebApplicationException(404);
        }
        flow.setApiKey(apiKey);
        List<SMSFlow> flows = flowMap.get(apiKey);
        flows.set(flow.getId(), flow);
        flowMap.save();
        return flow;
    }
}