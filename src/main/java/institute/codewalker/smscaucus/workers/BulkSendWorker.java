package institute.codewalker.smscaucus.workers;

import institute.codewalker.smscaucus.model.ActionNetworkPerson;
import institute.codewalker.smscaucus.model.ActionNetworkTaggingResult;
import institute.codewalker.smscaucus.model.BulkSendStats;
import institute.codewalker.smscaucus.service.ActionNetworkService;
import institute.codewalker.smscaucus.utils.BulkSendStatusMap;
import institute.codewalker.smscaucus.utils.StringUtils;

import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created by zekes on 3/2/2018.
 */
public class BulkSendWorker extends Thread {

    private String twilioAccountSid;
    private String twilioAuthToken;
    private String twilioServiceId;
    private String from;
    private String actionNetworkApiKey;
    private String actionNetworkTagId;
    private String body;
    private BulkSendStats stats;

    private ActionNetworkService actionNetwork;
    private BulkSendStatusMap statsMap;

    public BulkSendWorker(String twilioAccountSid, String twilioAuthToken, String twilioServiceId, String from,
                          String actionNetworkApiKey, String actionNetworkTagId, String body, BulkSendStats stats,
                          ActionNetworkService anService, BulkSendStatusMap statsMap) {
        this.twilioAccountSid = twilioAccountSid;
        this.twilioAuthToken = twilioAuthToken;
        this.twilioServiceId = twilioServiceId;

        this.from = from;
        this.actionNetworkApiKey = actionNetworkApiKey;
        this.actionNetworkTagId = actionNetworkTagId;
        this.body = body;
        this.stats = stats;
        this.actionNetwork = anService;
        this.statsMap = statsMap;
    }

    @Override
    public void run() {
        stats.setStarted(new Date());
        List<String> personLinks = getAllPersonLinks();

        for (String personLink: personLinks) {
            String[] linkParts = personLink.split("/");
            try {
                actionNetwork.setApiKey(actionNetworkApiKey);
                ActionNetworkPerson person = actionNetwork.getPerson(linkParts[linkParts.length - 1]);
                if (person != null) {
                    sendMessageToPerson(person);
                } else {
                    stats.setErrors(stats.getErrors() + 1);
                }
                statsMap.save();
                Thread.sleep(500);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                break;
            }
        }
        stats.setFinish(new Date());
        statsMap.save();
    }

    private List<String> getAllPersonLinks() {
        stats.setPage(0);
        stats.setTotalPages(1);
        List<String> result = new ArrayList<>();
        do {
            try {
                stats.setPage(stats.getPage() + 1);
                actionNetwork.setApiKey(actionNetworkApiKey);
                ActionNetworkTaggingResult response = actionNetwork.getTaggings(actionNetworkTagId, stats.getPage());
                if (response != null) {
                    stats.setTotalPages(response.getTotalPages());
                    stats.setTotalPeople(response.getTotalRecords());
                    result.addAll(response.getPersonLinks());
                    Thread.sleep(500);
                } else {
                    stats.setErrMsg("Failed to load any people from that tag!");
                }
                statsMap.save();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                break;
            } catch (NotFoundException e) {
                e.printStackTrace();;
            }
        } while(stats.getPage() != stats.getTotalPages());
        return result;
    }

    private void sendMessageToPerson(ActionNetworkPerson person) {
        if (person.getPhoneNumber() != null) {
            Map<String, String> personValues = new HashMap<>();
            personValues.put("FIRSTNAME", person.getGivenName());
            personValues.put("LASTNAME", person.getFamilyName());
            String personalMessage = StringUtils.replaceValues(body, personValues);

            System.out.println("Sending message: " + personalMessage);
            stats.setMessagesSent(stats.getMessagesSent() + 1);
        /*
        try {
            Twilio.init(twilioAccountSid, twilioAuthToken);
            Message message = Message
                    .creator(new PhoneNumber(person.getPhoneNumber()), twilionAccountSid, body)
                    .create();
            if(message.getErrorCode() == null) { //null when sending is successful
                System.out.println("Successfully sent sms to: " + person.getPhoneNumber());
            } else {
                System.out.println("Failed to send to: " + person.getPhoneNumber() + ", please check your Twilio account dashboard for more information. ErrorCode "+message.getErrorCode());
            }
        } catch (com.twilio.exception.ApiException exception){
            System.out.println("Failed to send to: " + person.getPhoneNumber() + ", " + exception.getMessage());
        }
        */
        } else {
            stats.setMissingNumbers(stats.getMissingNumbers() + 1);
        }
    }
}
